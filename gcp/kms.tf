/*
  Key names cannot be re-used, so this will prevent usage of the same names.
*/
resource "random_id" "suffix" {
  byte_length = 5
}

resource "google_kms_crypto_key" "default" {
  count           = var.create_key ? 1 : 0
  name            = "gke-${var.cluster_name}-key-${random_id.suffix.hex}"
  key_ring        = var.key_ring_id
  rotation_period = var.key_rotation_period

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_kms_crypto_key_iam_member" "default" {
  count         = var.create_key ? 1 : 0
  crypto_key_id = google_kms_crypto_key.default[0].id
  role          = "roles/cloudkms.cryptoKeyEncrypterDecrypter"
  member        = "serviceAccount:service-${data.google_project.current.number}@container-engine-robot.iam.gserviceaccount.com"
}
