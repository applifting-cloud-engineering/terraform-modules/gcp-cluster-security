output "kms_key" {
  value = google_kms_crypto_key.default[0].id
}
