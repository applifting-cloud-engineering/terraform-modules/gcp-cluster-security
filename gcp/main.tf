data "google_client_config" "current" {}
data "google_project" "current" {}

locals {
  gcp_project = data.google_client_config.current.project
}
