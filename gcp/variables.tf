
variable "cluster_name" {
  description = "Name of the cluster where you want to allow firewall"
  type        = string
}

variable "firewall_rules" {
  description = "Firewall rules"
  type        = list(object({ name = string, proto = string, source = string }))
  default     = []
}

variable "network_name" {
  description = "The name or self_link of the network to attach this firewall to"
  type        = string
}

variable "create_key" {
  description = "Number of keys you want to create"
  type        = bool
  default     = false
}

variable "key_ring_id" {
  description = "The KeyRing that KMS CryptoKey belongs to. Format: 'projects/{{project}}/locations/{{location}}/keyRings/{{keyRing}}'"
  type        = string
}

variable "key_rotation_period" {
  description = "Every time this period passes, generate a new CryptoKeyVersion and set it as the primary."
  type        = string
  default     = null
}