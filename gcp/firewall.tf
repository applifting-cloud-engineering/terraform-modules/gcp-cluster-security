resource "google_compute_firewall" "allow_rule" {
  count   = length(var.firewall_rules)
  name    = "gke-${var.cluster_name}-${var.firewall_rules[count.index].name}"
  network = var.network_name

  allow {
    protocol = regex("(tcp|udp)/(.*)", var.firewall_rules[count.index].proto).0
    ports    = split(",", regex("(tcp|udp)/(.*)", var.firewall_rules[count.index].proto).1)
  }

  source_ranges = length(var.firewall_rules[count.index].source) > 0 ? split(",", var.firewall_rules[count.index].source) : []
}
