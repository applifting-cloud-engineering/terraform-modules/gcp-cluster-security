## Gcp-cluster-security

## Description

The module uses `google_compute_firewall` to allow rules for the firewall, will create a name with the format `gke-${var.cluster_name}-${var.firewall_rules[count.index].name}` allowing protocols and ports and if `source_ranges` are specified, the firewall will apply only to traffic that has source IP address in these ranges.

Firewall rules allow or deny ingress traffic to, and egress traffic from your instances. The traffic is blocked by the firewall by default unless some of the firewall rules are created to allow it. 

Therefore for all networks except the default network, you must create any firewall rules you need.

Also the module uses `google_kms_crypto_key` to create a Google CryptoKey, it will create a name with the format `gke-${var.cluster_name}-key-${random_id.suffix.hex}`, and you can create a new key ring with a given `key_ring_id`.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | Name of the cluster where you want to allow firewall | `string` | n/a | yes |
| <a name="input_create_key"></a> [create\_key](#input\_create\_key) | Number of keys you want to create | `bool` | `false` | no |
| <a name="input_key_ring_id"></a> [key\_ring\_id](#input\_key\_ring\_id) | KeyRing that this key belongs to. Format: 'projects/{{project}}/locations/{{location}}/keyRings/{{keyRing}}' | `string` | `""` | no |
| <a name="input_key_rotation_period"></a> [key\_rotation\_period](#input\_key\_rotation\_period) | Every time this period passes, generate a new CryptoKeyVersion and set it as the primary. | `any` | `null` | no |
| <a name="input_network_name"></a> [network\_name](#input\_network\_name) | The name or self_link of the network to attach this firewall to | `string` | n/a | yes |
| <a name="input_firewall_rules"></a> [firewall_rules](#input\_firewall_rules) | Firewall rules | `list(object({ name = string, proto = string, source = string }))` | `[]` | no |

## Network firewall_rules example 

```hcl

  firewall_rules = [
    {
      name   = "allow-http"
      proto  = "tcp/8080,8443"
      source = module.cidrs.dataplanes[local.cluster_name]
    }
  ]

```

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_kms_key"></a> [kms\_key](#output\_kms\_key) | kms_key to be later imported by terraform |

## Usage 

Calling module gcp-cluster-security

```hcl

module "gcp_security" {
  source = "gitlab.com/applifting-cloud-engineering/gcp-cluster-security/gcp"
  version = "0.1.0"

  cluster_name = local.cluster_name_<CLUSTER_NAME>
  network_name = module.vpc.network_name

  rules = [
    {
      name   = "allow-http"
      proto  = "tcp/8080,8443"
      source = module.cidrs.dataplanes[local.cluster_name_<CLUSTER_NAME>]
    }
  ]

```

## Resources

| Name | Type |
|------|------|
| [google_compute_firewall.allow-rule](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_kms_crypto_key.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/kms_crypto_key) | resource |
| [google_kms_crypto_key_iam_member.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/kms_crypto_key_iam_member) | resource |
| [random_id.suffix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |
| [google_client_config.current](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config) | data source |
| [google_project.current](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/project) | data source |
